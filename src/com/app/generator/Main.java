package com.app.generator;

import java.lang.String;
import java.util.Scanner;

import com.letterGenerator.HorizontalLetterGenerator;
import com.letterGenerator.LetterGenerator;
import com.letterGenerator.VerticalLetterGenerator;

public class Main {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		int scale;
		char[] letters = new char[]{'X', 'Y', 'Z'};
		Scanner userInput = new Scanner(System.in);


		while(true){
			System.out.print("Enter the number of scale :");
			scale = userInput.nextInt();
			System.out.print("Vertical \n\r");
	      
			LetterGenerator verticalLetterGenerator = new VerticalLetterGenerator(scale, letters);
	        verticalLetterGenerator.printLetterBoard();
	        
	        System.out.print("Horizontal \n\r");
	        LetterGenerator horizontalLetterGenerator = new HorizontalLetterGenerator(scale, letters);
	        horizontalLetterGenerator.printLetterBoard();
				
		}
		
		
    	


	}

}
